﻿//---------------------------------------------------------------------------
//
// 2-node producer-consumer coupling example
//
// prod (4 procs) - con (2 procs)
//
// entire workflow takes 8 procs (2 dataflow procs between prod and con)
//
// Tom Peterka
// Argonne National Laboratory
// 9700 S. Cass Ave.
// Argonne, IL 60439
// mdreher@anl.gov
//
//--------------------------------------------------------------------------

#include <decaf/decaf.hpp>
#include <decaf/data_model/pconstructtype.h>
#include <decaf/data_model/arrayfield.hpp>
#include <decaf/data_model/boost_macros.h>

#include <assert.h>
#include <math.h>
#include <mpi.h>
#include <map>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include <sys/time.h>

using namespace decaf;
using namespace std;


// consumer
void checkpoint(Decaf* decaf)
{
    vector< pConstructData > in_data;

    MPI_Comm local_com = decaf->local_comm_handle();
    int rank = decaf->local_comm_rank();

    int total_n_pos = 0;
    bool first_it = true;

    std::ofstream stats;

    while (decaf->get(in_data))
    {

        ArrayFieldf fieldPos = in_data[0]->getFieldData<ArrayFieldf>("pos");
        ArrayFieldf fieldForces = in_data[0]->getFieldData<ArrayFieldf>("forces");
        ArrayFieldf fieldSpeed = in_data[0]->getFieldData<ArrayFieldf>("speed");
        SimpleFieldi fieldIt = in_data[0]->getFieldData<SimpleFieldi>("it");
        if (fieldPos && fieldIt && fieldForces && fieldSpeed)
        {
            float* arraypos = fieldPos.getArray();
            float* arrayforces = fieldForces.getArray();
            float* arrayspeed = fieldSpeed.getArray();
            unsigned int size = fieldPos.getArraySize();
            int it = fieldIt.getData();
            int n_pos_ = size / 3;

            if(first_it)
            {
                MPI_Reduce(&n_pos_, &total_n_pos, 1, MPI_INT, MPI_SUM, 0, local_com);
                first_it = false;
                if(rank == 0)
                {
                    std::string filename = "checkpoint_times.csv";
                    stats.open(filename);
                    stats<<"It;elapsedIt"<<std::endl;
                }
            }

            if(rank == 0)
            {
                struct timeval begin;
                struct timeval end;
                gettimeofday(&begin, NULL);
                float* fullarraypos = (float*)(malloc(total_n_pos * 3 * sizeof(float)));
                float* fullarrayforces = (float*)(malloc(total_n_pos * 3 * sizeof(float)));
                float* fullarrayspeed = (float*)(malloc(total_n_pos * 3 * sizeof(float)));
                unsigned int offset = 0;

                // Copying local data
                memcpy(fullarraypos, arraypos, n_pos_ * 3 * sizeof(float));
                memcpy(fullarrayforces, arrayforces, n_pos_ * 3 * sizeof(float));
                memcpy(fullarrayspeed, arrayspeed, n_pos_ * 3 * sizeof(float));
                offset += n_pos_ * 3;
                // Receiving from the other processes.
                int nb_recep;
                MPI_Comm_size(local_com, &nb_recep);
                int i;
                for(i = 1; i < nb_recep; i++)
                {
                    MPI_Status status;
                    int nbytes; // number of bytes in the message

                    // Pos
                    MPI_Probe(i, 0, local_com, &status);
                    MPI_Get_count(&status, MPI_FLOAT, &nbytes);
                    MPI_Recv(fullarraypos + offset, nbytes, MPI_FLOAT, i, 0, local_com, MPI_STATUS_IGNORE);

                    // Forces
                    MPI_Probe(i, 1, local_com, &status);
                    MPI_Get_count(&status, MPI_FLOAT, &nbytes);
                    MPI_Recv(fullarrayforces + offset, nbytes, MPI_FLOAT, i, 1, local_com, MPI_STATUS_IGNORE);


                    // Speed
                    MPI_Probe(i, 2, local_com, &status);
                    MPI_Get_count(&status, MPI_FLOAT, &nbytes);
                    MPI_Recv(fullarrayspeed + offset, nbytes, MPI_FLOAT, i, 2, local_com, MPI_STATUS_IGNORE);

                    offset += nbytes;
                }

                stringstream ss;
                char* prefix = getenv("CHECKPOINT_FOLDER");
                if(prefix != NULL)
                {
                    string path(prefix);
                    ss<<path;
                }
                else
                    ss<<"./";
                ss<<"/checkpoint_"<<it<<".bin";
                //fprintf(stderr, "Saving %d floats.\n", size);
                FILE* save = fopen(ss.str().c_str(),"wb+");

                fwrite(arraypos, sizeof(float), size, save);
                fwrite(arrayforces, sizeof(float), size, save);
                fwrite(arrayspeed, sizeof(float), size, save);

                fclose(save);

                free(fullarraypos);
                free(fullarrayforces);
                free(fullarrayspeed);
                gettimeofday(&end, NULL);
                double elapsedTimeIt = (end.tv_sec - begin.tv_sec) * 1000.0;      // sec to ms
                elapsedTimeIt += (end.tv_usec - begin.tv_usec) / 1000.0;   // us to ms
                stats<<it;
                stats<<";"<<elapsedTimeIt;
                stats<<std::endl;
                stats.flush();

            }
            else
            {
                MPI_Send(arraypos, n_pos_ * 3, MPI_FLOAT, 0, 0, local_com);
                MPI_Send(arrayforces, n_pos_ * 3, MPI_FLOAT, 0, 1, local_com);
                MPI_Send(arrayspeed, n_pos_ * 3, MPI_FLOAT, 0, 2, local_com);
            }


        }
        else
            fprintf(stderr, "Error: null pointer in con\n");

    }

    // terminate the task (mandatory) by sending a quit message to the rest of the workflow
    fprintf(stderr, "checkpoint %d terminating\n", rank);
    decaf->terminate();

    if(rank == 0)
        stats.close();
}


void run(Workflow& workflow)                             // workflow
{
    MPI_Init(NULL, NULL);

    // create decaf
    Decaf* decaf = new Decaf(MPI_COMM_WORLD, workflow);

    /*int comm_size = decaf->local_comm_size();
    if(comm_size != 1)
    {
        fprintf(stderr, "ERROR: Checkpoint should be send with only 1 MPI rank.\n");
        MPI_Abort(MPI_COMM_WORLD, -1);
    }*/


    // run workflow node tasks
    // decaf simply tells the user whether this rank belongs to a workflow node
    // how the tasks are called is entirely up to the user
    // e.g., if they overlap in rank, it is up to the user to call them in an order that makes
    // sense (threaded, alternting, etc.)
    // also, the user can define any function signature she wants
    if (decaf->my_node("checkpoint"))
        checkpoint(decaf);


    // cleanup
    delete decaf;
    MPI_Finalize();
}

// test driver for debugging purposes
// this is hard-coding the no overlap case
int main(int argc,
         char** argv)
{
    Workflow workflow;
    Workflow::make_wflow_from_json(workflow, "wflow_gromacs.json");

    // run decaf
    run(workflow);

    return 0;
}
