import os
import sys
import getopt



def main(argv):
   name = ''
   nbNodes = 1
   dflow = False
   try:
      opts, args = getopt.getopt(argv,"hf:n:d",["hostfile=","nnodes="])
   except getopt.GetoptError as err:
      print '"Parsing error: ' + str(err)
      print 'generateRankFile.py -f <hostfile>'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'generateRankFile.py -f <hostfile>'
         sys.exit()
      elif opt in ("-f", "--hostfile"):
         name = arg
      elif opt in ("-n","--nnodes"):
         nbNodes = int(arg)
      elif opt in ("-d","--dflow"):
	     dflow = True
   if name == "":
       print 'Error: empty filename'
       print 'generateRankFile.py -f <hostfile>'
       sys.exit(2)

   print 'Inputfile is is ', name

   hostFile=open(name)
   hosts=hostFile.read()
   hosts=hosts.rstrip("\n")
   hosts=hosts.split("\n")
   
   print "Hosts available: " + str(hosts)
   print "Number of hosts available: " + str(len(hosts))
   print "Number of hosts requested: " + str(nbNodes)
   
   if(len(hosts) < nbNodes):
       print "ERROR: Not enough hosts available. Request less or provide more hosts."
       sys.exit(2)

   currentRank = 0
   gmxPerNode = 15
   rankfile=open("rankfile_python.txt","w")
   outhostfile=open("hostfile_simu.txt","w")   
   stagehostfile=open("hostfile_stage.txt","w") 
   #GMX ranks
   for i in range(0, nbNodes):
       outhostfile.write("%s slots=16\n" % hosts[i])
       for j in range(0, gmxPerNode):
           rankfile.write("rank %s=%s slot=%s\n" % (str(currentRank), hosts[i], str(j)))
           currentRank+=1

   #DFlow nodes
   if dflow:
     for i in range(0, nbNodes):
      rankfile.write("rank %s=%s slot=%s\n" % (str(currentRank), hosts[i], str(gmxPerNode)))
      currentRank+=1
   
   #Treatment, 4 process per nodes
   for i in range(0, 1):
      rankfile.write("rank %s=%s slot=%s\n" % (str(currentRank), hosts[-1], str(i)))
      currentRank+=1
   stagehostfile.write("%s slots=1" % hosts[-1])
   #DFlow
   #if dflow:
   #   rankfile.write("rank %s=%s slot=%s\n" % (str(currentRank), hosts[-1], str(13)))
   #   currentRank+=1
   
   #TargetMAnager
   #rankfile.write("rank %s=%s slot=%s\n" % (str(currentRank), hosts[-1], str(14)))
   #currentRank+=1

   #DFlow
   #if dflow:
   #   rankfile.write("rank %s=%s slot=%s\n" % (str(currentRank), hosts[-1], str(15)))
   #   currentRank+=1

   rankfile.close()
   outhostfile.close()

if __name__ == "__main__":
   main(sys.argv[1:])
