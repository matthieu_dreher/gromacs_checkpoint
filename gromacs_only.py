# a small 2-node example, just a producer and consumer

# --- include the following 4 lines each time ---

import networkx as nx
import os
import imp
import argparse

wf = imp.load_source('workflow', os.environ['DECAF_PREFIX'] + '/python/workflow.py')

# --- set your options here ---

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
wf.initParserForTopology(parser)
args = parser.parse_args()

# Creating the topology
topo = wf.topologyFromArgs(args)
subtopos = topo.splitTopology(["gmx"],[4])


w = nx.DiGraph()
w.add_node("gmx", topology=subtopos[0], func='gmx',
    cmdline='./gromacs-4.5/bin/mdrun_mpi_4.5.5_decaf -s /home/matthieu/INRIA/molecules/FEPA/holo314.tpr -v -npme 0 -ncheck 0 -noddecaf -dlb yes')

# --- convert the nx graph into a workflow data structure and run the workflow ---

##wf.workflowToJson(w, mod_path, "wflow_gromacs.json")
#wf.workflowToSh(w, "wflow_gromacs.sh")
wf.processGraph(w, "wflow_gromacs")
