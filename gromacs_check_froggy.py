# a small 2-node example, just a producer and consumer

# --- include the following 4 lines each time ---

import networkx as nx
import os
import imp
import argparse

wf = imp.load_source('workflow', os.environ['DECAF_PREFIX'] + '/python/workflow.py')

# --- set your options here ---

# path to .so module for dataflow callback functions
mod_path = '/home/PROJECTS/pr-exaviz/Matthieu/Gromacs_check/install/bin/libmod_dflow_check.so'

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
wf.initParserForTopology(parser)
args = parser.parse_args()

# Creating the topology
topo = wf.topologyFromArgs(args)
subtopos = topo.splitTopology(["gmx","dflow","checkpoint"],[4,1,1])

w = nx.DiGraph()
w.add_node("gmx", topology=subtopos[0], func='gmx', cmdline='./gromacs-4.5/bin/mdrun_mpi_4.5.5_decaf -s /home/matthieu/INRIA/molecules/FEPA/holo314.tpr -v -npme 0 -ncheck 0 -ddecaf -nstepdecaf 50')
w.add_node("checkpoint", topology=subtopos[2], func='checkpoint', cmdline='./bin/checkpoint')

w.add_edge("gmx", "checkpoint", topology=subtopos[1], func='dflow_simple', path=mod_path,
           prod_dflow_redist='proc', dflow_con_redist='proc', cmdline='./bin/dflow_check')


# --- convert the nx graph into a workflow data structure and run the workflow ---

##wf.workflowToJson(w, mod_path, "wflow_gromacs.json")
wf.processGraph(w, "wflow_gromacs")
