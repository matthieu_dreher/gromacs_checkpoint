This document uses the [Markdown](http://daringfireball.net/projects/markdown/) syntax.

Gromacs_checkpoint is a workflow based on Decaf to checkpoint data extracted from Gromacs. This framework allows users to compare performance 
between time partitioning and space partitioning methods. When using a space partitioning strategy, the framework also allow users to select 
a flow control policy between the simulation and the writer. Experimental results are available in the 
article Manala: a Flexible Flow Control Library for Asynchronous Task Communication

# Dependencies

- Decaf
- FFTW

Decaf is available on [bitbucket](https://bitbucket.org/tpeterka1/decaf/). 

FFTW can be compiled with the following instructions:
```
cd $HOME/software
wget http://www.fftw.org/fftw-3.3.6-pl2.tar.gz
tar -xf fftw-3.3.6-pl2.tar.gz
cd fftw-3.3.6-pl2
./configure --prefix $HOME/software/install/ -enable-shared --enable-float --enable-sse2
make -j4 install
```

# Project Compilation

Retrieve the sources of the project (in the current directory, e.g.):
```
git clone https://bitbucket.org/matthieu_dreher/gromacs_checkpoint.git .
```

The project is built using CMake. Assuming that you created a build directory, then:
```
cd path/to/project/build
```

The following command builds the project. We recommend saving this into a script and running the script:

```
cmake path/to/project/source/ \
-DCMAKE_INSTALL_PREFIX:PATH=/path/to/project/install  \
-DDECAF_PREFIX:PATH=/path/to/decaf/install \
-DFFTW_PREFIX:PATH=/path/to/fftw/install
```

During the compilation 

# Testing the installation

The project includes 2 molecular models to test the installation. The following commands run the tests:

```
cd /path/to/project/install
#Small model, ideal for laptop
python gromacs_check_peptide_4.6.py -n 6
./wflow_gromacs.sh
# The simulation should last 5000 steps
# CTRL+C if the simulation last too long

cd /path/to/project/install
python gromacs_check_fepa_4.5.py -n 6
./wflow_gromacs.sh
# CTRL+C if the simulation last too long

```

In both cases, files with the name checkpoint_XX.bin should have appeared. 

# Simulation arguments

The project downloads, patches, and installs Gromacs 4.5.5 and Gromacs 4.6.7. Both versions of Gromacs have been modified 
to take into account several new parameters:

```
-[no]ddecaf  bool   yes     Activate Decaf operations
-nstepdecaf  int    500     Output decaf data every nstepdecaf iteration
-ncheck      int    0       Native checkpoint frequency
```

These parameters control how data are checkpointed by the framework. If -ddecaf is set to yes, the simulation will send data 
asynchronously to the writer (module checkpoint) every nstepdecaf iterations.
If ncheck is set to another value than 0, the simulation will write data synchronously every ncheck iterations.
Note that this parameter is independant from the parameter ddecaf: the simulation can both write data synchronously and send
data to the writer. In both the examples, the simulation sends data asynchronously to the writer and do not write data synchronously.
The user can change these parameters in their associated python files and try new configurations.

# Manala Configuration

TODO



